package com.arun.aashu.material_animationd3;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        Animation animation = AnimationUtils.loadAnimation(this, R.anim.translate);
        findViewById(R.id.splashImage).setAnimation(animation);

        getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Home.this, MainActivity.class));
                finish();
            }
        },5000);




    }
}
